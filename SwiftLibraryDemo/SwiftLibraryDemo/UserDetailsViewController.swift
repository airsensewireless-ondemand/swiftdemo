//
//  UserDetailsViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 08/03/2017.
//  Copyright © 2017 Airsense Wireless. All rights reserved.
//

class UserDetailsViewController: BaseViewController
{
    var appDelegate:AppDelegate?
    
    // user detail fields
    @IBOutlet var FirstNameTextField: UITextField!
    @IBOutlet var LastNameTextField: UITextField!
    @IBOutlet var PhoneNumberTextField: UITextField!
    @IBOutlet var PostCodeTextField: UITextField!
    @IBOutlet var TestKeyTextField: UITextField!
    @IBOutlet var TextValueTextView: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // get our app Delegate
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        FirstNameTextField.delegate = self
        LastNameTextField.delegate = self
        PhoneNumberTextField.delegate = self
        PostCodeTextField.delegate = self
        TestKeyTextField.delegate = self
        // TextValueTextView.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func performNewUserRegistration(result: AirSenseRegistration)
    {
        if ( result == RegOk )
        {
            // everything is okay, go to the status view
            performSegueOnMainThread( segueName: "segueUserDetailsToStatus" )
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Registration failed, try again!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func SubmitButtonAction(_ sender: UIButton)
    {
        // create the user details object and then try to register the user...
        
        let userDets = UserDetails()
        
        userDets.setUserDetailValue(FirstNameTextField.text, forKey: "First Name")
        userDets.setUserDetailValue(LastNameTextField.text, forKey: "Last Name")
        userDets.setUserDetailValue(PhoneNumberTextField.text, forKey: "Phone Number")
        userDets.setUserDetailValue(PostCodeTextField.text, forKey: "Post Code")
        userDets.setUserDetailValue(TextValueTextView.text, forKey: TestKeyTextField.text)
        
        if let username = appDelegate?.libAirSense?.getLogin()
        {
            appDelegate?.performRegistration(userName: username, userDetails: userDets, handler: performNewUserRegistration)
        }
    }
}
