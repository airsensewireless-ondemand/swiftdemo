//
//  EventTableViewCell.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 25/04/2017.
//  Copyright © 2017 Airsense Wireless. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var baseView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        baseView.backgroundColor = UIColor.lightGray
    }

}
