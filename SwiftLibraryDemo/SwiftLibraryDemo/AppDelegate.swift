//
//  AppDelegate.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

import UIKit
import CoreData

struct app_event_data
{
    var title = ""
    var time = ""
    var status = ""
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var libAirSense: LibAirSenseObjc?

    var onRegistrationConnection:SlotConnection?
    var onRegistrationFailed:SlotConnection?
    var onSyncConnection:SlotConnection?
    var onInternetAvaliableConnection:SlotConnection?
    var onHTTPAuthComplete:SlotConnection?
    
    var tableCellData = [app_event_data]()
    
    var autoLogin = true

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // [AirSense] initialise the library with an appropriate step timer value
        libAirSense = LibAirSenseObjc.init(stepInterval: 1)
        
        onRegistrationConnection        = libAirSense?.add( onRegistrationCallback: registrationSuccess )
        onRegistrationFailed            = libAirSense?.add( onRegistrationFailedCallback: registrationFailed )
        onSyncConnection                = libAirSense?.add( onSyncCallback: syncComplete )
        onInternetAvaliableConnection   = libAirSense?.add( onInternetAvailableCallback: internetAvaliableResult )
        onHTTPAuthComplete              = libAirSense?.add( onHttpAuthCallback: HTTPAuthComplete )
        
        return true
    }

    func checkUserDetails(eulaNeededHandler: (Void) -> Void, loginCheckHandler: (Void) -> Void)
    {
        if ( libAirSense?.isRegistrationAllowed() )!
        {
            loginCheckHandler()
        }
        else
        {
            eulaNeededHandler()
        }
    }
    
    func checkUserIsAuthenticated() -> Bool
    {
        return (libAirSense?.isAuthComplete())!
    }
    
    func checkLoginAvaliable(userName: String, handler: @escaping (AirSenseRegistrationCheck) -> Void)
    {
        self.libAirSense?.isUserRegistered(userName, withCallback: handler)
    }
    
    func validateUserCredentials(userName: String, password: String, handler: @escaping (AirSenseCredentialsCheck) -> Void)
    {
        self.libAirSense?.validateCredentials(userName, password: password, withCallback: handler);
    }
    
    func performLogin(userName: String, handler: @escaping (AirSenseRegistration) -> Void)
    {
        // login the user if they are already registered
        self.libAirSense?.loginUser(userName, onResult: handler)
    }
    
    func performRegistration(userName: String, userDetails: UserDetails, handler: @escaping (AirSenseRegistration) -> Void)
    {
        // the user name is valid so lets try to login
        // this will also save the login and password values for later use
        self.libAirSense?.registerUser(userName, havingOffersAndNews: true, userDetails: userDetails, onResult: handler);
    }
    
    func saveLogin(userName: String)
    {
        self.libAirSense?.setLogin(userName, password: "", emailSubscription: false)
    }
    
    func switchOffAutoLogin()
    {
        autoLogin = false
    }
    
    func shouldAutoLogin() -> Bool
    {
        return autoLogin
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        libAirSense?.onEnterBackground()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        libAirSense?.onEnterForeground()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        if #available(iOS 10.0, *) {
            self.saveContext()
        } else {
            // Fallback on earlier versions
        }
    }

    func getCurrentTimeAsString() -> String
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    func registrationSuccess(result: AirSenseRegistration)
    {
        var data = app_event_data()
        
        data.title    = "Registration Complete"
        data.time     = getCurrentTimeAsString()
        data.status   = "Success"
        
        tableCellData.append(data)
    }
    
    func registrationFailed(result: AirSenseRegistration)
    {
        var data = app_event_data()
        
        data.title    = "Registration Complete"
        data.time     = getCurrentTimeAsString()
        data.status   = "Failed"
        
        tableCellData.append(data)
    }
    
    func syncComplete()
    {
        var data = app_event_data()
        
        data.title    = "Sync Complete"
        data.time     = getCurrentTimeAsString()
        data.status   = "Unknown"
        
        tableCellData.append(data)
    }
    
    func internetAvaliableResult(result: Bool)
    {
        var data = app_event_data()
        
        data.title    = "Internet Check Complete"
        data.time     = getCurrentTimeAsString()
        
        if ( result )
        {
            data.status   = "Avaliable"
        }
        else
        {
            data.status   = "Unavaliable"
        }
        
        tableCellData.append(data)
    }
    
    func HTTPAuthComplete(result: Bool)
    {
        var data = app_event_data()
        
        data.title    = "HTTP Auth Complete"
        data.time     = getCurrentTimeAsString()
        
        if ( result )
        {
            data.status   = "Success"
        }
        else
        {
            data.status   = "Failiure"
        }
        
        tableCellData.append(data)
    }
    
    func getEventData() -> [app_event_data]
    {
        return tableCellData
    }
    
    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "SwiftLibraryDemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    @available(iOS 10.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

