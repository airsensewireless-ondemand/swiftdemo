//
//  LoginViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 10/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController
{
    var appDelegate:AppDelegate?
    
    // login page
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var busyView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // get our app Delegate
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        if let username = appDelegate?.libAirSense?.getLogin()
        {
            if ( userNameField != nil && username != "Unset" )
            {
                userNameField.text = username
            }
        }
        
        if let password = appDelegate?.libAirSense?.getPassword()
        {
            if ( passwordField != nil && password != "Unset"  )
            {
                passwordField.text = password
            }
        }
        
        userNameField.delegate = self
        passwordField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if ( appDelegate?.libAirSense == nil )
        {
            // something has gone badly wrong, log it and hang!
            print("Could not find the appDelegate in LoginViewController!");
        }
        else
        {
            if ( (userNameField.text?.isEmpty)! || (passwordField.text?.isEmpty)! || !(appDelegate?.shouldAutoLogin())! )
            {
                busyView.isHidden = true
            }
            else
            {
                busyView.isHidden = false
                appDelegate?.validateUserCredentials(userName: userNameField.text!, password: passwordField.text!, handler: validateResult)
            }
        }
    }
    
    func passwordWrong()
    {
        busyView.isHidden = true
        passwordField.text = ""
        
        let alert = UIAlertController(title: "Alert", message: "Password was wrong", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func malformedEmail()
    {
        busyView.isHidden = true
        userNameField.text = ""
        
        let alert = UIAlertController(title: "Alert", message: "Email address is not valid!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func error()
    {
        busyView.isHidden = true
        userNameField.text = ""
        
        let alert = UIAlertController(title: "Alert", message: "An unknown error has occured, please try again!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func unknownUser()
    {
        changeViewToUserDetailsView()
    }
    
    func loginRequired()
    {
        busyView.isHidden = true
    }
    
    func validateResult(result: AirSenseCredentialsCheck)
    {
        if ( result == CredChkOk )
        {
            if ( appDelegate?.checkUserIsAuthenticated() )!
            {
                changeViewToStatusView()
            }
            else
            {
                busyView.isHidden = false
                appDelegate?.performLogin(userName: userNameField.text!, handler: loginResult)
            }
        }
        else if ( result == CredChkWrongLoginPwd )
        {
            busyView.isHidden = true
        }
        else
        {
            // no idea what went wrong, try again after a delay?
            delay(5)
            {
                self.busyView.isHidden = false
                self.appDelegate?.validateUserCredentials(userName: self.userNameField.text!, password: self.passwordField.text!, handler: self.validateResult)
            }
        }
    }
    
    func loginResult(result: AirSenseRegistration)
    {
        if ( result == RegOk )
        {
            // everything is okay, go to the status view
            changeViewToStatusView()
        }
        else if ( result == RegWrongLoginPwd )
        {
            passwordWrong()
        }
        else if ( result == RegMalformedEmail )
        {
            malformedEmail()
        }
        else if ( result == RegLoginNotFound )
        {
            // the email is  a new user:
            unknownUser()
        }
        else
        {
            error();
        }
    }
    
    func userNameCheckResult(result: AirSenseRegistrationCheck)
    {
        if ( result == RegChkAlreadyRegistered )
        {
            // known user, try to login with the password given
            appDelegate?.performLogin(userName: userNameField.text!, handler: loginResult)
        }
        else if ( result == RegChkLoginAvailable )
        {
            // new user, check the password on the new user page
            // [OJ] Test new password-less login
            appDelegate?.saveLogin(userName: userNameField.text!)
            changeViewToUserDetailsView()
        }
        else if ( result == RegChkMalformedEmail )
        {
            malformedEmail()
        }
        else
        {
            // no idea what went wrong, try again after a delay?
            delay(5)
            {
                self.busyView.isHidden = false
                self.appDelegate?.checkLoginAvaliable(userName: self.userNameField.text!, handler: self.userNameCheckResult)
            }
        }
    }
    
    @IBAction func loginButtonAction(_ sender: AnyObject)
    {
        busyView.isHidden = false
        
        // check whether the username matches a known one or we are seeing a new user/malformed email
        appDelegate?.checkLoginAvaliable(userName: self.userNameField.text!, handler: userNameCheckResult)
    }
    
    func changeViewToNewUserView()
    {
        performSegueOnMainThread(segueName: "segueLoginToNewUser" )
    }
    
    func changeViewToStatusView()
    {
        performSegueOnMainThread( segueName: "segueLoginToStatus" )
    }
    
    func changeViewToUserDetailsView()
    {
        performSegueOnMainThread( segueName: "segueLoginToUserDetails" )
    }
}
