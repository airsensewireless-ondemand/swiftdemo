//
//  EventsViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

class EventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK: Properties
    @IBOutlet var eventTableView: UITableView!
    
    var appDelegate:AppDelegate?
    
    var cellIdentifier = "EventTableViewCell"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        // eventTableView.register(EventTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        eventTableView.delegate = self
        eventTableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return appDelegate!.getEventData().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // var cell:UITableViewCell = .dequeueReusableCell(withIdentifier: "cell")!
        
        guard let cell = eventTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? EventTableViewCell  else
        {
            fatalError("The dequeued cell is not an instance of an EventTableViewCell.")
        }
        
        let event_data = appDelegate!.getEventData()[indexPath.row]
            
        cell.titleLabel.text    = event_data.title
        cell.timeLabel.text     = event_data.time
        cell.statusLabel.text   = event_data.status
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You selected cell #\(indexPath.row)!")
    }
     
    
}
