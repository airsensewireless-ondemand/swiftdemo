//
//  NewUserViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 10/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

import UIKit

class NewUserViewController: BaseViewController
{
    var appDelegate:AppDelegate?
    
    // new user page
    @IBOutlet weak var newUserNameField: UITextField!
    @IBOutlet weak var newPasswordOneField: UITextField!
    @IBOutlet weak var newPasswordTwoField: UITextField!
    
    @IBOutlet weak var busyView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // get our app Delegate
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        if let username = appDelegate?.libAirSense?.getLogin()
        {
            if ( newUserNameField != nil )
            {
                newUserNameField.text = username
            }
        }
        
        if let password = appDelegate?.libAirSense?.getPassword()
        {
            if ( newPasswordOneField != nil )
            {
                newPasswordOneField.text = password
            }
            
            if ( newPasswordTwoField != nil )
            {
                newPasswordTwoField.text = ""
            }
        }
        
        newUserNameField.delegate = self
        newPasswordOneField.delegate = self
        newPasswordTwoField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        busyView.isHidden = true
    }
    
    func performNewUserRegistration(result: AirSenseRegistration)
    {
        if ( result == RegOk )
        {
            // everything is okay, go to the status view
            changeViewToStatusView()
        }
        else if ( result == RegMalformedEmail )
        {
            newUserNameField.text = ""
            busyView.isHidden = true
            
            let alert = UIAlertController(title: "Alert", message: "Email not valid!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            newPasswordOneField.text = ""
            newPasswordTwoField.text = ""
            busyView.isHidden = true
            
            let alert = UIAlertController(title: "Alert", message: "Unknown error, please enter a new password and try again!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func NewUserSubmitAction(_ sender: AnyObject)
    {
        // check the passwords match
        if ( newPasswordOneField.text != newPasswordTwoField.text )
        {
            newPasswordTwoField.text = ""
            busyView.isHidden = true
            
            let alert = UIAlertController(title: "Alert", message: "Passwords do no match!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            busyView.isHidden = false
            /*
            appDelegate?.performLogin(userName: newUserNameField.text!, password: newPasswordOneField.text!, handler: performNewUserRegistration )
            */
        }
    }
    
    func changeViewToStatusView()
    {
        performSegueOnMainThread( segueName: "segueNewUserToStatus" )
    }

}
