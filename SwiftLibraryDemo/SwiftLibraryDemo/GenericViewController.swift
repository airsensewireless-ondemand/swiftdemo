//
//  ViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

import UIKit

class GenericViewController: BaseViewController
{
    var appDelegate:AppDelegate?
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet var WifiLabel: UILabel!
    @IBOutlet var InternetStatusLabel: UILabel!
    @IBOutlet var serverLabel: UILabel!
    @IBOutlet var networkTagLabel: UILabel!
    
    // main page
    
    var onInternetAvaliableSlotConnection: SlotConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // get our app Delegate
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        // Thanks stackoverflow: Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        onInternetAvaliableSlotConnection = appDelegate?.libAirSense?.add(onInternetAvailableCallback: self.updateInternetStatus)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptEULAAction(_ sender: AnyObject)
    {
        appDelegate?.libAirSense?.allowRegistration(true)
        performSegueOnMainThread( segueName: "segueEULAToLogin" )
    }

    @IBAction func lohoutAction(_ sender: Any)
    {
        appDelegate?.switchOffAutoLogin()
        performSegueOnMainThread( segueName: "segueStatusToLogin" )
    }
    
    func updateInternetStatus(avaliable:Bool)
    {
        if serverLabel != nil && networkTagLabel != nil
        {
            serverLabel.text = appDelegate?.libAirSense?.getServerApi();
            networkTagLabel.text = appDelegate?.libAirSense?.getNetworkTag();
        }
        
        if InternetStatusLabel != nil && WifiLabel != nil
        {
            if (avaliable)
            {
                InternetStatusLabel.text = "Internet Avaliable"
            }
            else
            {
                InternetStatusLabel.text = "Internet Unavaliable"
            }
            
            if let netInfo = appDelegate?.libAirSense?.currentNetworkInfo()
            {
                
                if ( netInfo.isWifi )
                {
                    switch(netInfo.type)
                    {
                    case NetTypeManaged:
                        WifiLabel.text = netInfo.ssid + " [Managed Network]";
                        break;
                    case NetTypeNative:
                        WifiLabel.text = netInfo.ssid;
                        break;
                    case NetTypeNotConnected:
                        WifiLabel.text = "WiFi is associated but not connected yet";
                        break;
                    default:
                        WifiLabel.text = "Error";
                    }
                    
                }
                else if ( netInfo.isMobile )
                {
                    WifiLabel.text = "Cellular connection detected."
                }
                else
                {
                    WifiLabel.text = "No connection wifi or cellular connection detected!"
                }
            }
            else
            {
                WifiLabel.text = "No connection information avaliable!"
            }
        }
    }
}

