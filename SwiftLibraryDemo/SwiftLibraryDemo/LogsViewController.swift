//
//  LogsViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

class LogsViewController: UIViewController, UITextViewDelegate
{
    @IBOutlet weak var outputTextView: UITextView!
    var logTimer: Timer!
    var isScrolling: Bool!
    var lastNumLines: Int!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        outputTextView.text = "..."
        outputTextView.layoutManager.allowsNonContiguousLayout = false
        outputTextView.delegate = self
        
        isScrolling = false
        lastNumLines = 0
        
        logTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(updateTextView), userInfo: nil, repeats: true)
    }
    
    func updateTextView()
    {
        let documentsDirectory  = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let logFile             = documentsDirectory + "/airsense_log.txt"
        
        do
        {
            let logText         = try String(contentsOfFile: logFile, encoding: String.Encoding.utf8)
            let lines           = logText.components(separatedBy: CharacterSet.newlines)
            
            // [OJ] Don't update if the log hasn't changed
            if ( lines.count == lastNumLines )
            {
                return
            }
            
            lastNumLines        = lines.count
            
            var finalString     = ""
            
            for line in lines
            {
                if let range: Range<String.Index> = line.range(of: "]")
                {
                    finalString += line.substring(from: range.upperBound)
                }
                else
                {
                    finalString += line
                }
                
                finalString += "\n"
            }
            
            outputTextView.text = finalString
            
            if ( !isScrolling )
            {
                let stringLength:Int = outputTextView.text.characters.count
                outputTextView.scrollRangeToVisible( NSMakeRange(stringLength - 1, 0) )
            }
        }
        catch
        {
            outputTextView.text = "Error reading log file."
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if ( scrollView.contentOffset.y >= ( scrollView.contentSize.height - scrollView.frame.size.height ) )
        {
            // debugPrint("Scrolled to bottom")
            isScrolling = false
        }
        else
        {
            isScrolling = true
        }
    }
    
}
