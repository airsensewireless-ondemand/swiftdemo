//
//  InitialViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

import UIKit

class InitialViewController: BaseViewController
{
    var appDelegate:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // get our app Delegate
        appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if ( appDelegate?.libAirSense == nil )
        {
            // something has gone badly wrong, hang!
        }
        else
        {
            appDelegate?.checkUserDetails(eulaNeededHandler: changeViewToEULAView, loginCheckHandler: changeViewToLoginView)
        }
    }

}
