//
//  BaseViewController.swift
//  SwiftLibraryDemo
//
//  Created by Oliver Jauncey on 07/10/2016.
//  Copyright © 2016 Airsense Wireless. All rights reserved.
//

class BaseViewController: UIViewController, UITextFieldDelegate
{
    func performSegueOnMainThread(segueName: String)
    {
        OperationQueue.main.addOperation {
            [weak self] in
            self?.performSegue(withIdentifier: segueName, sender: self)
        }
    }
    
    func changeViewToLoginView()
    {
        performSegueOnMainThread( segueName: "segueStartToLogin" )
    }
    
    func changeViewToEULAView()
    {
        performSegueOnMainThread( segueName: "segueStartToEULA" )
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard()
    {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        view.endEditing(true)
        return false
    }
    
    // generic function to delay execution
    func delay(_ delay:Double, closure:@escaping ()->())
    {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}
